package ru.demo.runnablethread;

/**
 * Класс, реализующий интерфейс Runnable
 * @author Т.А.Инюшкина
 */

public class NewThread implements Runnable {
    //Ссылка на объект потока
    Thread thread;

    public NewThread() {
        // Создание объекта потока
        thread = new Thread(this, "Новый поток");
        System.out.println(thread);
        // Запуск потока
        thread.start();
    }

    @Override
    public void run() {
        System.out.println("Дочерний поток запущен");
        for (int i = 5; i > 0; i--) {
            System.out.println(i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("Дочерний поток прерван");
            }
        }
        System.out.println("Дочерний поток завершен");
    }
}
