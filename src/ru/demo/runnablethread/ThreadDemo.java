package ru.demo.runnablethread;

/**
 * @author Т.А.Инюшкина
 */

public class ThreadDemo {
    public static void main(String[] args) {
        System.out.println("Главный поток запущен");
        new NewThread(); // Создание анонимного объекта класса NewThread
        for (int i = 100; i > 90; i--) {
            System.out.println(i);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                System.out.println("Главный поток прерван");
            }
        }
        System.out.println("Главный поток завершен");
    }
}
