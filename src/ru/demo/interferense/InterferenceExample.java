package ru.demo.interferense;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author ITA
 */

public class InterferenceExample {
    private static final int HUNDRED_MILLION = 100000000;
    private AtomicInteger counter = new AtomicInteger();

    public boolean stop(){
        return counter.incrementAndGet() > HUNDRED_MILLION;
    }

    public void example() throws InterruptedException {
        InterferenceThread thread1 = new InterferenceThread(this);
        InterferenceThread thread2 = new InterferenceThread(this);
        thread1.start();
        thread2.start();
        thread1.join();
        System.out.println("Expected: " + HUNDRED_MILLION);
        System.out.println("Result: " + thread1.detI());


    }

}
