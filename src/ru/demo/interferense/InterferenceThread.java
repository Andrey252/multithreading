package ru.demo.interferense;

/**
 * @author ITA
 */

public class InterferenceThread extends Thread {
    private final InterferenceExample checker;
    private static volatile int i;
//    private static Object lock = new Object();

    public InterferenceThread(InterferenceExample checker) {
        this.checker = checker;
    }

    //synchronized static
    public void increment() {
        synchronized (checker) {
            i++;
        }
    }

    public int detI() {
        return i;
    }

    public void run() {
        while (!checker.stop()) {
            increment();
        }
    }
}
